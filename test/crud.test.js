const { postTodo, updateTodo, deleteTodo, getTodos } = require("./services");
const { createTodo } = require("./utils");
describe("CRUD Tests", () => {
    let todo = createTodo({
        todo: 'Test'
    })
    test("Add todo", async() => {
        var error = null

        try{
            const response = await postTodo(todo);
            expect(todo.id).toBe(response.id)  
        }catch(err){
            error = true            
        }
        expect(error).toBeNull()
        
        
    })
    test("Update todo", async () => {
        var error = null
        var t = 'TestUpdate'
        try{
            const response = await updateTodo({ ...todo, todo: t});
            expect(t).toBe(response.todo);
        }catch(err){
            error = true            
        }
        expect(error).toBeNull()
        
    })
    test("Read todo", async () => {
        var error = null
        try{
            const response = await getTodos();
            expect(1).toEqual(response.filter(t=>t.id == todo.id).length)
        }catch(err){
            error = true            
        }
        expect(error).toBeNull()

    })
    test("Delete todo", async () => {
        
        var error = null
        try{
            const response = await deleteTodo(todo.id);
            
        }catch(err){
            console.log(err)
            error = true            
        }
        expect(error).toBeNull()
        
        
    })
});