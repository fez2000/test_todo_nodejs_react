const axios = require("axios");

const port = 3001;
axios.defaults.baseURL = `http://localhost:${port}`;
axios.defaults.headers.post["Content-Type"] = "application/json";

module.exports.getTodos =  function getTodos() {
  return axios
    .get("/todos")
    .then((response) => response.data)
    .catch((error) => error.message);
}

exports.getLabels = function getLabels() {
  return axios
    .get("/labels")
    .then((response) => response.data)
    .catch((error) => error.message);
}

exports.postTodo = function postTodo(data) {
  return axios
    .post("/todos", data)
    .then((response) => response.data)
    .catch((error) => error.message);
}

exports.postLabel = function postLabel(data) {
  return axios
    .post("/labels", data)
    .then((response) => response.data)
    .catch((error) => error.message);
}

exports.searchTodo = function searchTodo(stringQuery, label) {
  return axios
    .get(
      `/todos?todo_like=${stringQuery}${
        label === "all" ? "" : `&label=${label}`
      }`
    )
    .then((response) => response.data)
    .catch((error) => error.message);
}

exports.deleteTodo = function deleteTodo(id) {
  return axios
    .delete(`/todos/${id}`)
    .then((response) => response.data)
    .catch((error) => error.message);
}

exports.deleteLabel = function deleteLabel(id) {
  return axios
    .delete(`/labels/${id}`)
    .then((response) => response.data)
    .catch((error) => error.message);
}

exports.updateTodo = function updateTodo(data) {
  return axios
    .put(`/todos/${data.id}`, data)
    .then((response) => response.data)
    .catch((error) => error.message);
}

exports.updateLabel = function updateLabel(data) {
  return axios
    .put(`/labels/${data.id}`, data)
    .then((response) => response.data)
    .catch((error) => error.message);
}
