const crypto = require('crypto');


exports.createUniqueId = function createUniqueId() {
  return crypto.randomUUID().slice(0, 8);
}

exports.createTodo = function createTodo(data) {
  let { todo, label } = data;

  return {
    todo: todo,
    label: label,
    id: module.exports.createUniqueId(),
    date: new Date().toLocaleString(),
    isDone: false,
  };
}

exports.createLabel = function createLabel(data) {
  let { label, count } = data;

  return {
    label: label,
    count: count,
    id: createUniqueId(),
  };
}

exports.findLabel = function findLabel(label, list) {
  return list.find((item) => item.label === label);
}
